uncle_ben = User.create email: 'uncle_ben@yummy.com', password: 'bananas'
auntie_clara = User.create email: 'auntie@clara.us', password: 'paospaos'

casserole = %{
INGREDIENTS
1 28-ounce can chicken in water, drained
1 cup frozen green peas
1 14-ounce can cream of chicken soup
1 pouch UNCLE BEN’S READY RICE Original Long Grain Rice

Chicken Rice Casserole
Servings: 4
Prep time: 5–15 minutes
ABOUT THIS DISH
This easy-to-prepare recipe may look simple, but chicken and rice never tasted so deliciously satisfying. When you’re short on time but long for a hearty meal, this is it!
INSTRUCTIONS
Combine all ingredients in microwave safe bowl.
Cover and heat for 4 minutes, stir and serve.
}
bens_casserole = uncle_ben.recipes.create title: 'Chicken Rice Casserole', directions: casserole
auntie_clara.comments.create body: 'I love how easy it is to prepare this dish!', recipe: bens_casserole