Rails.application.routes.draw do

  post '/api/v1/recipes' => 'api/v1/recipes#create'
  get '/api/v1/recipes/:recipe_id' => 'api/v1/recipes#show'
  get '/api/v1/recipes' => 'api/v1/recipes#index'
  put '/api/v1/recipes/:recipe_id' => 'api/v1/recipes#update'
  patch '/api/v1/recipes/:recipe_id' => 'api/v1/recipes#update'
  delete '/api/v1/recipes/:recipe_id' => 'api/v1/recipes#delete'

  post '/api/v1/recipes/:recipe_id/comments' => 'api/v1/comments#create'
  get '/api/v1/comments/:comment_id' => 'api/v1/comments#show'
  put '/api/v1/comments/:comment_id' => 'api/v1/comments#update'
  patch '/api/v1/comments/:comment_id' => 'api/v1/comments#update'
  delete '/api/v1/comments/:comment_id' => 'api/v1/comments#delete'

end
