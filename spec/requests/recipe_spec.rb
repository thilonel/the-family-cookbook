require 'rails_helper'

RSpec.describe "recipe" do

  let!(:user) { User.create email: 'a@a.com', password: '123456' }
  let!(:user_credentials) { ActionController::HttpAuthentication::Basic.encode_credentials(user.id, user.password) }
  let!(:user2) { User.create email: 'b@b.com', password: 'asdasd' }
  let!(:admin) { User.create email: 'admin@tfc.com', password: 'soSecure', admin: true }
  let!(:admin_credentials) { ActionController::HttpAuthentication::Basic.encode_credentials(admin.id, admin.password) }
  let!(:recipe) { user.recipes.create title: 'a', directions: 'b' }
  let!(:comment1) { user.comments.create body: 'asd', recipe: recipe }
  let!(:comment2) { user2.comments.create body: 'qwe', recipe: recipe }
  let!(:user2_recipe) { user2.recipes.create title: 'asd', directions: 'dfg' }


  describe "list" do
    it "returns ids of all recipes in an array" do
      get "/api/v1/recipes"

      expect(response.status).to eq 200
      expect(parsed_body).to eq(Recipe.select(:id).all.pluck(:id))
      expect(response.headers["Content-Type"]).to match /application\/json/
    end

    it "returns empty array when no recipes are present" do
      Recipe.delete_all
      get "/api/v1/recipes"

      expect(response.status).to eq 200
      expect(parsed_body).to eq([])
      expect(response.headers["Content-Type"]).to match /application\/json/
    end
  end

  describe "create" do
    it "is successful" do
      post "/api/v1/recipes", {recipe: {title: "T", directions: "D"}}.to_json,
           {'Content-Type' => "application/json", 'Authorization' => user_credentials}

      expect(response.status).to eq 201
      expect(Recipe.last[:title]).to eq("T")
      expect(Recipe.last[:directions]).to eq("D")
      expect(Recipe.last[:user_id]).to eq user.id
      expect(response.headers["Location"]).to match /\/api\/v1\/recipes\/\d+/
    end

    it "returns validation errors" do
      post "/api/v1/recipes", nil,
           {'Content-Type' => "application/json", 'Authorization' => user_credentials}

      expect(response.status).to eq 400
      expect(parsed_body).to eq({message: 'Creation failed', errors: [title: ["can't be blank"], directions: ["can't be blank"]]})
      expect(response.headers["Content-Type"]).to match /application\/json/
    end

    it "forbids to create if unauthenticated" do
      post "/api/v1/recipes", nil,
           {'Content-Type' => "application/json"}

      expect(response.status).to eq 401
    end

    it "allows creation for admins" do
      post "/api/v1/recipes", nil,
           {'Content-Type' => "application/json", 'Authorization' => admin_credentials}

      expect(response.status).not_to eq 401
      expect(response.status).not_to eq 403
    end
  end


  describe "read" do
    it "is successful" do
      get "/api/v1/recipes/#{recipe.id}"

      expected = recipe.slice(:id, :title, :directions, :user_id).symbolize_keys
        .merge(comments: recipe.comments.pluck(:id))

      expect(response.status).to eq 200
      expect(parsed_body).to eq expected
      expect(response.headers["Content-Type"]).to match /application\/json/
    end

    it "returns recipe not found" do
      get "/api/v1/recipes/999"

      expect(response.status).to eq 404
      expect(parsed_body).to eq({message: "Couldn't find Recipe with 'id'=999"})
      expect(response.headers["Content-Type"]).to match /application\/json/
    end
  end


  describe "update" do
    %w(put patch).each do |method|
      it "#{method}: is successful" do
        public_send method, "/api/v1/recipes/#{recipe.id}",
                    {recipe: {title: "a", directions: 'X'}}.to_json,
                    {'Content-Type' => "application/json", 'Authorization' => user_credentials}

        expect(response.status).to eq 200
        expect(recipe.reload.directions).to eq 'X'
      end

      it "#{method}: returns recipe not found" do
        public_send method, "/api/v1/recipes/999", nil,
                    {'Content-Type' => "application/json", 'Authorization' => user_credentials}

        expect(response.status).to eq 404
        expect(parsed_body).to eq({message: "Couldn't find Recipe with 'id'=999"})
        expect(response.headers["Content-Type"]).to match /application\/json/
      end

      it "#{method}: returns bad request and validation errors" do
        public_send method, "/api/v1/recipes/#{recipe.id}",
                    {recipe: {title: nil, directions: nil}}.to_json,
                    {'Content-Type' => 'application/json', 'Authorization' => user_credentials}

        expect(response.status).to eq 400
        expect(parsed_body).to eq({message: 'Update failed', errors: [title: ["can't be blank"], directions: ["can't be blank"]]})
        expect(response.headers["Content-Type"]).to match /application\/json/
      end

      it "#{method}: can not modify user_id" do
        public_send method, "/api/v1/recipes/#{recipe.id}", {recipe: {user_id: user2.id}}.to_json,
                    {'Content-Type' => "application/json", 'Authorization' => user_credentials}

        expect(response.status).to eq 200
        expect(recipe.user).to eq user
      end

      it "#{method}: forbids to modify someone else's recipe" do
        public_send method, "/api/v1/recipes/#{user2_recipe.id}", nil,
                    {'Content-Type' => "application/json", 'Authorization' => user_credentials}

        expect(response.status).to eq 403
      end

      it "#{method}: forbids to modify if unauthenticated" do
        public_send method, "/api/v1/recipes/#{recipe.id}", nil,
                    {'Content-Type' => "application/json"}

        expect(response.status).to eq 401
      end

      it "#{method}: allows modification for admins" do
        public_send method, "/api/v1/recipes/#{recipe.id}", nil,
                    {'Content-Type' => "application/json", 'Authorization' => admin_credentials}

        expect(response.status).not_to eq 401
        expect(response.status).not_to eq 403
      end
    end
  end


  describe "delete" do
    let!(:comment) { Comment.create body: 'b', recipe: recipe }

    it "is successful" do
      delete "/api/v1/recipes/#{recipe.id}", nil,
             {'Content-Type' => "application/json", 'Authorization' => user_credentials}

      expect(response.status).to eq 200
      expect(Recipe.find_by_id recipe.id).to be_nil
      expect(Comment.find_by_id comment.id).to be_nil
    end

    it "returns recipe not found" do
      delete "/api/v1/recipes/999", nil,
             {'Content-Type' => "application/json", 'Authorization' => user_credentials}

      expect(response.status).to eq 404
      expect(parsed_body).to eq({message: "Couldn't find Recipe with 'id'=999"})
      expect(response.headers["Content-Type"]).to match /application\/json/
    end

    it "forbids to delete someone else's recipe" do
      delete "/api/v1/recipes/#{user2_recipe.id}", nil,
             {'Content-Type' => "application/json", 'Authorization' => user_credentials}

      expect(response.status).to eq 403
    end

    it "forbids to delete if unauthenticated" do
      delete "/api/v1/recipes/#{recipe.id}", nil, {'Content-Type' => "application/json"}

      expect(response.status).to eq 401
    end

    it "allows access for admins" do
      delete "/api/v1/recipes/#{recipe.id}", nil,
             {'Content-Type' => "application/json", 'Authorization' => admin_credentials}

      expect(response.status).not_to eq 401
      expect(response.status).not_to eq 403
    end
  end

end