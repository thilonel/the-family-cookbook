require 'rails_helper'

RSpec.describe "api" do

  before(:all) do
    Rails.application.routes.draw do
      post '/api/v1/test' => 'api/v1/test#index'
      put '/api/v1/test' => 'api/v1/test#index'
      patch '/api/v1/test' => 'api/v1/test#index'
      get '/api/v1/test' => 'api/v1/test#index'
      delete '/api/v1/test' => 'api/v1/test#index'
    end
  end
  after(:all) { Rails.application.reload_routes! }

  class Api::V1::TestController < ApplicationController
    def index
      head :ok
    end
  end

  context "content-type restriction" do
    let(:user) { User.create email: 'a@a.com', password: 'asdf' }

    it "post" do
      post "/api/v1/test"
      expect(response.status).to eq 415
      expect(parsed_body).to eq({message: "Content-Type should be application/json"})
    end

    it "put" do
      put "/api/v1/test"
      expect(response.status).to eq 415
      expect(parsed_body).to eq({message: "Content-Type should be application/json"})
    end

    it "patch" do
      put "/api/v1/test"
      expect(response.status).to eq 415
      expect(parsed_body).to eq({message: "Content-Type should be application/json"})
    end

    it "get" do
      get "/api/v1/test"
      expect(response.status).to eq 200
    end

    it "delete" do
      delete "/api/v1/test"
      expect(response.status).to eq 200
    end
  end

end
