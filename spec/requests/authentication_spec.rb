require 'rails_helper'

RSpec.describe "authentication" do

  before(:all) { Rails.application.routes.draw { get '/api/v1/users/:user_id/dummies' => 'api/v1/dummies#index' } }
  after(:all) { Rails.application.reload_routes! }

  class Api::V1::DummiesController < ApplicationController
    before_action :authenticate
    def index
      render body: current_user
    end
  end


  let!(:user) { User.create email: 'a@a.com', password: '123456' }

  it "sets the current user if credentials are valid" do
    credentials = ActionController::HttpAuthentication::Basic.encode_credentials(user.id, user.password)
    get "/api/v1/users/#{user.id}/dummies", nil, {'Authorization' => credentials}

    expect(response.body).not_to be_empty
  end

  it "returns unauthorized if password is invalid" do
    credentials = ActionController::HttpAuthentication::Basic.encode_credentials(user.id, 'asdasd')
    get "/api/v1/users/#{user.id}/dummies", nil, {'Authorization' => credentials}

    expect(response.status).to eq 401
  end

  it "return unauthorized if user id is invalid" do
    credentials = ActionController::HttpAuthentication::Basic.encode_credentials(999, user.password)
    get "/api/v1/users/#{user.id}/dummies", nil, {'Authorization' => credentials}

    expect(response.status).to eq 401
  end

end
