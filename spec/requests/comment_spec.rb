require 'rails_helper'

RSpec.describe "comment" do
  let!(:user) { User.create email: 'a@a.com', password: '123456' }
  let!(:user_credentials) { ActionController::HttpAuthentication::Basic.encode_credentials(user.id, user.password) }
  let!(:user2) { User.create email: 'b@b.com', password: 'asdasd' }
  let!(:user2_credentials) { ActionController::HttpAuthentication::Basic.encode_credentials(user2.id, user2.password) }
  let!(:admin) { User.create email: 'admin@tfc.com', password: 'soSecure', admin: true }
  let!(:admin_credentials) { ActionController::HttpAuthentication::Basic.encode_credentials(admin.id, admin.password) }
  let!(:recipe) { user.recipes.create title: 't', directions: 'd' }
  let!(:comment) { recipe.comments.create body: 'b', user: user }

  describe "create" do
    it "is successful" do
      post "/api/v1/recipes/#{recipe.id}/comments",
           {comment: {body: "X"}}.to_json,
           {'Content-Type' => "application/json", 'Authorization' => user_credentials}

      expect(response.status).to eq 201
      expect(Comment.last[:body]).to eq("X")
      expect(Comment.last[:user_id]).to eq user.id
      expect(Comment.last[:recipe_id]).to eq recipe.id
      expect(response.headers["Location"]).to match /\/api\/v1\/recipes\/#{recipe.id}\/comments\/\d+/
    end

    it "returns validation errors" do
      post "/api/v1/recipes/#{recipe.id}/comments", nil,
           {'Content-Type' => "application/json", 'Authorization' => user_credentials}

      expect(response.status).to eq 400
      expect(parsed_body).to eq({message: 'Creation failed', errors: [body: ["can't be blank"]]})
      expect(response.headers["Content-Type"]).to match /application\/json/
    end

    it "returns recipe not found" do
      post "/api/v1/recipes/999/comments", nil,
           {'Content-Type' => "application/json", 'Authorization' => user_credentials}

      expect(response.status).to eq 404
      expect(parsed_body).to eq({message: "Couldn't find Recipe with 'id'=999"})
    end

    it "forbids to create if unauthenticated" do
      post "/api/v1/recipes/#{recipe.id}/comments", nil, {'Content-Type' => "application/json"}

      expect(response.status).to eq 401
    end
  end


  describe "read" do
    it "is successful" do
      get "/api/v1/comments/#{comment.id}"

      expect(response.status).to eq 200
      expect(parsed_body).to eq({id: 1, body: 'b', recipe_id: recipe.id, user_id: user.id})
      expect(response.headers["Content-Type"]).to match /application\/json/
    end

    it "returns comment not found" do
      get "/api/v1/comments/999"

      expect(response.status).to eq 404
      expect(parsed_body).to eq({message: "Couldn't find Comment with 'id'=999"})
    end
  end


  describe "update" do
    %w(put patch).each do |method|
      specify "#{method}: success" do
        public_send method, "/api/v1/comments/#{comment.id}",
                    {comment: {body: 'Y'}}.to_json,
                    {'Content-Type' => "application/json", 'Authorization' => user_credentials}

        expect(response.status).to eq 200
        expect(comment.reload.body).to eq 'Y'
      end

      it "#{method}: returns comment not found" do
        public_send method, "/api/v1/comments/999", nil,
                    {'Content-Type' => "application/json", 'Authorization' => user_credentials}

        expect(response.status).to eq 404
        expect(parsed_body).to eq({message: "Couldn't find Comment with 'id'=999"})
      end

      it "#{method}: returns validation errors" do
        public_send method, "/api/v1/comments/#{comment.id}",
                    {comment: {body: nil}}.to_json,
                    {'Content-Type' => "application/json", 'Authorization' => user_credentials}

        expect(response.status).to eq 400
        expect(parsed_body).to eq({message: 'Update failed', errors: [body: ["can't be blank"]]})
        expect(response.headers["Content-Type"]).to match /application\/json/
      end

      it "#{method}: returns bad request when trying to move comment between recipes" do
        public_send method, "/api/v1/comments/#{comment.id}",
                    {comment: {body: 'b', recipe_id: 999}}.to_json,
                    {'Content-Type' => "application/json", 'Authorization' => user_credentials}

        expect(response.status).to eq 400
        expect(parsed_body).to eq({message: 'Update failed', errors: [recipe: ["not modifiable"]]})
      end

      it "forbids to modify if unauthenticated" do
        public_send method, "/api/v1/comments/#{comment.id}", nil, {'Content-Type' => "application/json"}

        expect(response.status).to eq 401
      end

      it "forbids to edit other's comments" do
        public_send method, "/api/v1/comments/#{comment.id}", nil,
                    {'Content-Type' => "application/json", 'Authorization' => user2_credentials}

        expect(response.status).to eq 403
      end

      it "allows update for admins" do
        public_send method, "/api/v1/comments/#{comment.id}", nil,
                    {'Content-Type' => "application/json", 'Authorization' => admin_credentials}

        expect(response.status).not_to eq 401
        expect(response.status).not_to eq 403
      end
    end
  end


  describe "delete" do
    it "is successful" do
      delete "/api/v1/comments/#{comment.id}", nil,
             {'Content-Type' => "application/json", 'Authorization' => user_credentials}

      expect(response.status).to eq 200
      expect { Comment.find(comment.id) }.to raise_error ActiveRecord::RecordNotFound
    end

    it "returns comment not found" do
      delete "/api/v1/comments/999", nil,
             {'Content-Type' => "application/json", 'Authorization' => user_credentials}

      expect(response.status).to eq 404
      expect(parsed_body).to eq({message: "Couldn't find Comment with 'id'=999"})
    end

    it "forbids to delete if unauthenticated" do
      delete "/api/v1/comments/#{comment.id}"

      expect(response.status).to eq 401
    end

    it "forbids to delete other's comments" do
      delete "/api/v1/comments/#{comment.id}", nil,
             {'Content-Type' => "application/json", 'Authorization' => user2_credentials}

      expect(response.status).to eq 403
    end

    it "allows delete for admins" do
      delete "/api/v1/comments/#{comment.id}", nil,
             {'Content-Type' => "application/json", 'Authorization' => admin_credentials}

      expect(response.status).not_to eq 401
      expect(response.status).not_to eq 403
    end
  end

end