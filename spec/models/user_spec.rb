require 'rails_helper'

RSpec.describe User do

  it { is_expected.to validate_presence_of(:email) }

  describe "before_save" do
    let(:password) { 'asdf' }
    subject(:user) { user = User.create email: 'a@a.com', password: password }

    it { expect(user.salt).not_to be_nil }
    it { expect(user.admin).to be(false) }
    it { expect(user.password).not_to eq password }
    it { expect(user.password).not_to be_nil }
  end

end
