require 'rails_helper'

RSpec.describe Comment do

  it { is_expected.to validate_presence_of(:body) }
  it { is_expected.to validate_presence_of(:recipe) }
  it { is_expected.to validate_presence_of(:user) }

end
