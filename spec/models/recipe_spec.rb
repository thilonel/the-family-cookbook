require 'rails_helper'

RSpec.describe Recipe do

  it { is_expected.to validate_presence_of(:title) }
  it { is_expected.to validate_presence_of(:directions) }

  it "deletes it's comments" do
    recipe = Recipe.create title: 't', directions: 'd'
    comment1 = recipe.comments.create body: 'b1'
    comment2 = recipe.comments.create body: 'b2'

    recipe.destroy
    expect{Comment.find(comment1.id)}.to raise_error(ActiveRecord::RecordNotFound)
    expect{Comment.find(comment2.id)}.to raise_error(ActiveRecord::RecordNotFound)
  end

end
