class Comment < ActiveRecord::Base

  belongs_to :recipe, inverse_of: :comments
  belongs_to :user, inverse_of: :comments

  validates :body, :recipe, :user, presence: true

end
