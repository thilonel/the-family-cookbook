class Recipe < ActiveRecord::Base

  belongs_to :user, inverse_of: :recipes
  has_many :comments, inverse_of: :recipe, dependent: :delete_all

  validates :title, :directions, presence: true

end
