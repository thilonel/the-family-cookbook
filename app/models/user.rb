class User < ActiveRecord::Base

  has_many :recipes, inverse_of: :user
  has_many :comments, inverse_of: :user

  validates :email, presence: true

  before_save :set_default_values

  private

  def set_default_values
    self.admin ||= false
    self.salt = BCrypt::Engine.generate_salt
    self.password = BCrypt::Engine.hash_secret(self.password, self.salt)
  end

end
