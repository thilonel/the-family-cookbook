module Finders

  extend ActiveSupport::Concern



  def recipe
    not_found { @recipe ||= Recipe.find params[:recipe_id] }
  end



  def comment
    not_found { @comment ||= Comment.find params[:comment_id] }
  end

end