module EnsureContentType

  extend ActiveSupport::Concern

  included do
    before_action :ensure_content_type
  end



  def ensure_content_type
    return if (%w(GET DELETE).include?(request.method) || request.content_type == "application/json")
    render json: {message: "Content-Type should be application/json"}, status: :unsupported_media_type
  end

end