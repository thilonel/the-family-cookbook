module AutomaticReturn

  extend ActiveSupport::Concern

  included do
    around_action :catch_halt
  end



  def render(*args)
    super
    throw :halt
  end



  def head(*args)
    super
    throw :halt
  end



  def catch_halt
    catch :halt do
      yield
    end
  end

end