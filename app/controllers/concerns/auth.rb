module Auth

  extend ActiveSupport::Concern

  included do
    attr_reader :current_user
  end



  def authenticate
    authenticate_or_request_with_http_basic do |user_id, password|
      @current_user = User.find user_id
      current_user.password == password
    end
  rescue ActiveRecord::RecordNotFound
    request_http_basic_authentication
  end



  def authorize(resource)
    head :forbidden unless @current_user == resource.user || @current_user.admin
  end

end