module Responses

  extend ActiveSupport::Concern



  def not_found
    yield
  rescue ActiveRecord::RecordNotFound => e
    render json: {message: e.message}, status: :not_found
  end



  def validation_failed(resource, message)
    render json: {message: message, errors: [resource.errors]}, status: :bad_request
  end



  def unmodifiable_attribute(attribute)
    render json: {message: 'Update failed', errors: [attribute.to_sym => ["not modifiable"]]}, status: :bad_request
  end

end
