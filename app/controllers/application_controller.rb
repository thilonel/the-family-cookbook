class ApplicationController < ActionController::Base

  include AutomaticReturn
  include EnsureContentType
  include Auth
  include Responses

end
