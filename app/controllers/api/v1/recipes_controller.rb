class Api::V1::RecipesController < ApplicationController

  include Finders
  before_action :authenticate, only: [:create, :update, :delete]
  before_action -> { authorize recipe }, only: [:update, :delete]



  def index
    render json: Recipe.select(:id).all.pluck(:id)
  end



  def create
    recipe = current_user.recipes.create recipe_params
    head :created, location: "/api/v1/recipes/#{recipe.id}" if recipe.valid?
    validation_failed(recipe, 'Creation failed')
  end



  def show
    comment_ids = recipe.comments.pluck :id
    render json: recipe.slice(:id, :title, :directions, :user_id).merge(comments: comment_ids)
  end



  def update
    recipe.update recipe_params
    head :ok if recipe.valid?
    validation_failed(recipe, 'Update failed')
  end



  def delete
    recipe.destroy
    head :ok
  end



  private

  def recipe_params
    params.fetch(:recipe, {}).permit(:title, :directions)
  end

end
