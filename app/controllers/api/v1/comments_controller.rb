class Api::V1::CommentsController < ApplicationController

  include Finders
  before_action :authenticate, only: [:create, :update, :delete]
  before_action ->{ authorize comment }, only: [:update, :delete]



  def create
    comment = current_user.comments.new(recipe: recipe, body: comment_params[:body])
    head :created, location: "/api/v1/recipes/#{recipe.id}/comments/#{comment.id}" if comment.save
    validation_failed(comment, 'Creation failed')
  end



  def show
    render json: comment.slice(:id, :body, :user_id, :recipe_id)
  end



  def update
    unmodifiable_attribute 'recipe' if comment_params[:recipe_id].present?
    comment.update(comment_params)
    head :ok if comment.valid?
    validation_failed(comment, 'Update failed')
  end



  def delete
    comment.delete
    head :ok
  end



  private

  def comment_params
    params.fetch(:comment, {}).permit(:body, :recipe_id)
  end

end
